import numpy as np
import requests as requests
from bs4 import BeautifulSoup

TRAINING = 0
TESTING = 1


# https://stackoverflow.com/questions/11023530/python-to-list-http-files-and-directories/34718858
def get_url_paths(url, ext=''):
    response = requests.get(url)
    if response.ok:
        response_text = response.text
    else:
        return response.raise_for_status()
    soup = BeautifulSoup(response_text, 'html.parser')
    parent = [url + node.get('href') for node in soup.find_all('a') if node.get('href').endswith(ext)]
    return parent


# https://www.kite.com/python/answers/how-to-replace-the-last-occurence-of-a-character-in-a-string-in-python
def identify_row_data(row, subject, ext):
    subject_num = subject[subject.rfind("s") + 1:len(subject) - 1]
    img_num = row[row.rfind("/") + 1:row.rfind(ext) - 1]
    # https://stackoverflow.com/questions/10648490/removing-first-appearance-of-word-from-a-string
    img_link = row.replace(subject, "https://resources.csed22.com", 1)
    return subject_num, img_num, img_link


def identify_orl_data_set(url, ext):
    dataset = []
    subjects = get_url_paths(url)
    for subject in subjects:
        if "orl/s" in subject:
            subject = subject.replace("/data/iss/orl", "", 1)
            data = get_url_paths(subject, ext)
            for row in data:
                subject_num, img_num, img_link = identify_row_data(row, subject, ext)
                dataset.append([subject_num, img_num, img_link])
    return dataset


# https://stackoverflow.com/questions/55711159/pandas-read-csv-from-url-and-include-request-header/55711425
def get_img_content(url, image_header_size):
    req = requests.get(url)
    # Convert every image into a vector of 10,304 values corresponding to the image size
    return list(req.content[image_header_size:])


def read_orl_data_set(dataset, sample_split):
    data = [[], []]
    label = [[], []]

    for row in dataset:
        img_content = get_img_content(url=row[2], image_header_size=14)
        if int(row[1]) % sample_split:
            data[0].append(img_content)
            label[0].append(row[0])
        else:
            data[1].append(img_content)
            label[1].append(row[0])

    # https://stackoverflow.com/questions/46051977/what-is-the-default-dtype-for-str-like-input-in-numpy
    data = {
        "TRAINING": np.array(data[0], dtype='B'),
        "TESTING": np.array(data[1], dtype='B')
    }
    label = {
        "TRAINING": np.array(label[0], dtype=int),
        "TESTING": np.array(label[1], dtype=int)
    }
    return data, label
