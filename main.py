from utils import *
from sklearn import metrics
from sklearn.neighbors import KNeighborsClassifier

dataset = identify_orl_data_set('https://resources.csed22.com/data/iss/orl', 'pgm')
data, label = read_orl_data_set(dataset, sample_split=2)

COV = np.cov(data["TRAINING"].T, ddof=0)
w, v = np.linalg.eigh(COV)

# https://stackoverflow.com/questions/6771428/most-efficient-way-to-reverse-a-numpy-array
eigenvalue = w[::-1]
eigenvector = v[::-1]

eigen_sum = np.sum(eigenvalue, 0)

# Note: 100% alpha is always at (n-1)
alpha = [0.8, 0.85, 0.9, 0.95, 1]
eigen_num = []
for a in alpha:
    for i in range(eigenvalue.size):
        if np.sum(eigenvalue[0:i + 1], 0) / eigen_sum >= a:
            eigen_num.append(i + 1)
            break
print("Eigens: ", eigen_num, "\n")

projection_matrix = []
for e in eigen_num:
    pm = np.zeros((e, 10304))
    for i in range(e):
        # Note: had to truncate the imaginary part, sk-knn does not support it.
        pm[i] = eigenvector[int(i)]
    projection_matrix.append(pm.T)

for i in range(len(alpha)):
    proj_training_data = np.matmul(data["TRAINING"], projection_matrix[i])
    proj_testing_data = np.matmul(data["TESTING"], projection_matrix[i])

    scores = []

    # https://towardsdatascience.com/knn-using-scikit-learn-c6bed765be75
    for k in range(1, 8, 2):
        knn = KNeighborsClassifier(n_neighbors=k)
        knn.fit(proj_training_data, label["TRAINING"])
        pred = knn.predict(proj_testing_data)
        scores.append(metrics.accuracy_score(pred, label["TESTING"]))

    print("alpha =", alpha[i])
    print(np.round(scores, 3))
